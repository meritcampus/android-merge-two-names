package com.example.mergetwonames;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	EditText firstName, secondName;
	Button addButton;
	TextView resultText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		firstName = (EditText) findViewById(R.id.first_edittext);
		secondName = (EditText) findViewById(R.id.second_edittext);
		addButton = (Button) findViewById(R.id.add_button);
		resultText = (TextView) findViewById(R.id.resultText);
		addButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.add_button:
			if(firstName.getText().toString().length() >0&&secondName.getText().toString().length() >0){
				resultText.setVisibility(View.VISIBLE);
			
			resultText.setText(firstName.getText().toString() + " " +secondName.getText().toString());
			}else{
				
				Toast.makeText(getApplicationContext(), "Fields should not be empty", Toast.LENGTH_SHORT).show();
			}

			break;

		default:
			break;
		}
	}
}